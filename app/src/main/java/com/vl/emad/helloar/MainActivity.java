package com.vl.emad.helloar;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final Double MIN_OPENGL_VERSION = 3.0 ;
    ArFragment arFragment;
    ModelRenderable robotRenderable;

    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIfSupportedDeviceOrFinish(this)){
            return;
        }
        setContentView(R.layout.activity_main);

        arFragment = (ArFragment)getSupportFragmentManager()
                .findFragmentById(R.id.ux_fragment);

        ModelRenderable.builder()
                .setSource(this,Uri.parse(""))
                .build()
                .thenAccept(renderable -> robotRenderable = renderable)
                .exceptionally( throwable -> {
                    Toast
                            .makeText(this,
                                    "unable to load renderable",
                                    Toast.LENGTH_LONG)
                            .show();
                    return null;
                });


        arFragment.setOnTapArPlaneListener((HitResult hitresult, Plane plane, MotionEvent motionevent) -> {

            if (robotRenderable == null) {return;}

            Anchor anchor = hitresult.createAnchor();
            AnchorNode anchorNode = new AnchorNode(anchor);

            anchorNode.setParent(arFragment.getArSceneView().getScene());

            TransformableNode robot = new TransformableNode(arFragment.getTransformationSystem());
            robot.setParent(anchorNode);
            robot.setRenderable(robotRenderable);
            robot.select();
        });

    }



    public static Boolean checkIfSupportedDeviceOrFinish(final Activity activity){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

            Log.e(TAG,"SceneForm requires android N and above");

            Toast.makeText(activity,"update your device to android N or later",
                    Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }

        String openGLVersionString = ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                .getDeviceConfigurationInfo()
                .getGlEsVersion();

        if (Double.parseDouble(openGLVersionString)< MIN_OPENGL_VERSION) {
            Log.e(TAG, "SceneForm requires OpenGL 3.0 or later");
            Toast.makeText(activity,"SceneForm requires OpenGL 3.0 or later",Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }

        return true;
    }

}
